#!/usr/bin/env bash

bundle exec rake db:create
bundle exec rake db:migrate
exec bundle exec unicorn -c config/containers/unicorn.rb -E $RAILS_ENV --no-default-middleware;
#bundle exec puma -C ./config/containers/puma.rb